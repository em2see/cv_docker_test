from flask import Flask, __version__ as fversion
import numpy as np
import pyzbar
import cv2

app = Flask(__name__)

@app.route('/')
def hello_world():
    out = '<h1>Hello, World!</h1><br>'
    out += '<ul>'
    out += '<li>flask version:\t{:}</li>'.format(fversion)
    out += '<li>numpy version:\t{:}</li>'.format(np.__version__)
    out += '<li>pyzbar version:\t{:}</li>'.format(pyzbar.__version__)
    #out += '<li>cv2 version:\t{:}</li>'.format(cv2.__version__)
    out += '</ul>'
    return out
