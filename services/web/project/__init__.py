from flask import Flask, render_template, send_from_directory
from flask_socketio import SocketIO, emit
import os
import uuid

from pyzbar import pyzbar
import cv2
from PIL import Image
from io import BytesIO
from imutils.video import VideoStream,FileVideoStream
from base64 import b64encode
import numpy as np
from copy import copy

# for socketio
import eventlet
eventlet.monkey_patch()

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode='eventlet', cors_allowed_origins="*")

root_path = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "view"))

@app.route('/')
def root():
    return send_from_directory(root_path, 'scanner.html')
    
@app.route('/<path:path>')
def static_file(path):
    return send_from_directory(root_path, path)

@socketio.on('my_event', namespace='/socket.io')
def event_message(message):
    print('got message')
    emit('my_response', {'data': message['data']})

@socketio.on('video_event', namespace='/socket.io')
def video_message(message):
    #print('got message ', message['data'])
    file_path = os.path.join(root_path, 'img', str(uuid.uuid4())+'.tmp')
    with open(file_path,'wb') as fl:
        fl.write(message['data'])
    print(file_path)
    cap = cv2.VideoCapture(file_path)
    barcodes=[]
    stop=False
    while(cap.isOpened() and not stop):
        ret, frame = cap.read()
        if ret:
            barcodes.append(pyzbar.decode(frame)) 
        else:    
            stop = True
    #send message back
    print(len(barcodes))
    emit('video_response', {'data': barcodes_prepare(barcodes)})
    #remove file
    #os.remove(file_path)

@socketio.on('image_event', namespace='/socket.io')
def image_message(message):
    #
    #file_path = os.path.join(root_path, 'img', str(uuid.uuid4())+'.itmp')
    #with open(file_path,'wb') as fl:
    #    fl.write(message['data'])
    arr = np.frombuffer(message['data'],dtype=np.uint8)
    frame = cv2.imdecode(arr, -1)
    barcodes=[]
    barcodes.append(pyzbar.decode(frame)) 
    #send message back
    #print(len(barcodes))
    emit('video_response', {'data': barcodes_prepare(barcodes)})
    #remove file
    #os.remove(file_path)

def barcodes_prepare(barcodes):
    resp = []
    
    for codes in barcodes:
        tmp_codes = []
        for code in codes:
            tmp_code = {'data':'','rect':[],'poly':[],'type':''}
            tmp_code['data']=code.data.decode('utf-8')
            tmp_code['typ']=code.type
            tmp_code['poly']=[(point.x,point.y) for point in code.polygon]
            tmp_codes.append(tmp_code)
        resp.append(tmp_codes)
    print(resp)
    return resp

@socketio.on('my_broadcast_event', namespace='/socket.io')
def brodcast_message(message):
    emit('my_response', {'data': message['data']}, broadcast=True)

@socketio.on('connect', namespace='/socket.io')
def socket_connect():
    emit('connect_response', {'data': 'Connected'})

@socketio.on('disconnect', namespace='/socket.io')
def socket_disconnect():
    print('Client disconnected')

#socketio.on_namespace(MyNamespace('/socket.io'))    

if __name__ == '__main__':
    pass
    # app.run(host='localhost', port=9999, debug=True)
    #socketio.run(app,host='0.0.0.0',port=5000, certfile='cert.pem', keyfile='key.pem')
    #socketio.run(app,host='0.0.0.0',port=5000)
